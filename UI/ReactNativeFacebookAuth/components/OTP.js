import React, { useState, Component, } from "react";
import axios from "axios"
import { Button, View, Text, StyleSheet, TextInput, TouchableOpacity, Image } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import OTPTextView from 'react-native-otp-textinput';
import Imageupload from "./imageupload";

const products = [];

const setText = () => {
    otpInput.current.setValue("1234");
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        padding: 5,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        fontSize: 22,
        fontWeight: '500',
        textAlign: 'center',
        color: '#333333',
        marginBottom: 20,
    },
    textInputContainer: {
        marginBottom: 20,
    },
    roundedTextInput: {
        borderRadius: 10,
        borderWidth: 4,
    },
    buttonWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 20,
        width: '60%',
        background:"#05075d"
    },
    textInput: {
        height: 40,
        width: '80%',
        borderColor: '#000',
        borderWidth: 1,
        padding: 10,
        fontSize: 16,
        letterSpacing: 5,
        marginBottom: 10,
        textAlign: 'center',
    },
    buttonStyle: {
        marginHorizontal: 20,
    },
    button:{
        height:30,
        backgroundColor:"#05075d",
        width:120,
        borderRadius:12
    
      },
      buttonText:{
        alignContent:"center",
        textAlign:"center",
        fontSize:20,
        color:"#FFFFFF"
    
      },
      button1:{
        height:30,
        backgroundColor:"#05075d",
        width:120,
        borderRadius:12,
        right:20
    
      },
      buttonText1:{
        alignContent:"center",
        textAlign:"center",
        fontSize:20,
        color:"#FFFFFF"
    
      },
     
});
export default class Aboutscreen extends Component {
    customerid = (inputtext, data) => {
        this.setState({ C_ID: inputtext })
    }
    login = (C_ID) => {

        let data = {
            customerId: C_ID

        }
        axios.post("http://localhost:3000/product", data).then(resp => {
            let responsedata = {
                data:
                    [
                        {
                            _id: '',
                            C_ID: '',
                            name: '',
                            price: '',
                            rent: '',
                            loc: ''
                        }

                    ]
            }
            var dataconvert = '';
            dataconvert = JSON.stringify(resp);
            responsedata = JSON.parse(dataconvert);
            // show result
            var output = '';
            for (var i = 0; i < responsedata.data['data'].length; i++) {
                var bit = responsedata.data['data'][i];
                output += '[name: "' + bit['name'] +
                    '", price: "' + bit['price'] +
                    '", rent: "' + bit['rent'] +
                    '",loc:"' + bit['loc'] + ']\n';
            };
            products.push(output);
            alert(output.name);
            this.props.navigation.navigate("Imageupload")
        })
            .catch(err => {
                console.log(err);
            })
    }




    alertText = () => {
        const { otpInput = '' } = this.state;
        if (otpInput) {
            alert(otpInput);
        }
        this.props.navigation.navigate('Imageupload')

    };

    clear = () => {
        this.input1.clear();
    };

    updateOtpText = () => {
        // will automatically trigger handleOnTextChange callback passed
        this.input1.setValue(this.state.inputText);
    };
    state = {
        otpInput: '',
        inputText: '',

    };
    render() {
        const { navigation } = this.props;
        var otp = navigation.getParam('otp', '')
        //alert(this.otp);
        this.state.otpInput = otp;



        return (

            <View style={styles.container}>
                <Text style={styles.instructions}>Please Enter The Otp</Text>
                <OTPTextView
                    ref={(e) => (this.input1 = e)}
                    containerStyle={styles.textInputContainer}
                    handleTextChange={(text) => this.setState({ otpInput: text })}
                    inputCount={4}
                    textInputStyle={[styles.roundeTextInput, { borderRadius: 100 }]}
                    keyboardType="numeric"
                    defaultValue={this.state.otpInput}
                />
                <View style={styles.buttonWrapper}>
                    <TouchableOpacity
                     style={styles.button1}
                     title="Clear" 
                     onPress={this.clear} >
                          <Text style={styles.buttonText1}>Clear</Text>

                    </TouchableOpacity>
                    
                    <TouchableOpacity
                       style={styles.button}
                        title="Submit"
                        onPress={this.alertText}
                       
                    >
                         <Text style={styles.buttonText}>Submit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}