import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Productscreen from "./containers/Productscreen";
import ProductDetailsScreen from "./containers/ProductDetails";
import ShoppingCartIcon from "./containers/ShoppingCartIcon";
import CartScreen from "./containers/CartScreen";
import buttonScreen from "./containers/button";
import { MaterialIcons } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { EvilIcons } from "@expo/vector-icons";
import SearchIcon from "./containers/SearchIcon";
import HomeScreen from "./components/Homescreen";
import AboutScreen from "./components/Aboutscreen";
import cardetails from "./components/cardetails";
import OTP from "./components/OTP";
import Imageupload from "./components/imageupload"
export default class ShoppingCart extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const AppNavigator = createStackNavigator(
  {
    
    Home1: {
      screen: HomeScreen,
      navigationOptions: {
        header: null,
      }

    },
    Imageupload: {
      screen: Imageupload,
      navigationOptions: {
        header: null,
      }

    },
    Productscreen: {
      screen: Productscreen,
      navigationOptions: {
        title: "Discover",
        headerRight: <SearchIcon />,

        headerLeft: <Entypo name="menu" size={24} color="purple" />,
        headerTop: <Text>0</Text>,
        headerTintColor: "purple",
        headerTitleStyle: {
          fontWeight: "bold",
          textAlign: "center"
        }
      }
    },

    ProductDetails: {
      screen: ProductDetailsScreen,
      navigationOptions: {
        header: null
      }
    },

    Cart: {
      screen: CartScreen,
      navigationOptions: {
        headerTitle: "Discover",
        headerRight: <ShoppingCartIcon />
      }
    }
    ,


    cardetails: {
      screen: cardetails,
      navigationOptions: {
        header: null,
      }

    },
    OTP: {
      screen: OTP,
      navigationOptions: {
        header: null,
      }
    },
    About: {
      screen: AboutScreen,
      navigationOptions: {
        header: null,
      }
    },

  }

);
const AppContainer = createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
